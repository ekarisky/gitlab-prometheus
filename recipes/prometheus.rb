require "yaml"
include_recipe "gitlab-prometheus::default"

alertmanagers_search = node["prometheus"]["alertmanagers_search"]
alertmanagers_query = search(:node, alertmanagers_search).sort! { |a, b| a[:fqdn] <=> b[:fqdn] }
file node["prometheus"]["alertmanager"]["inventory"] do
  content generate_inventory_file(alertmanagers_query, node["prometheus"]["alertmanager"]["port"], []).to_yaml
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
end

directory node["prometheus"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory "#{Chef::Config[:file_cache_path]}/runbooks" do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

git "#{Chef::Config[:file_cache_path]}/runbooks" do
  repository node["prometheus"]["runbooks"]["git_http"]
  revision node["prometheus"]["runbooks"]["branch"]
  action :sync
  notifies :hup, "runit_service[prometheus]", :delayed
end

link node["prometheus"]["alerting_rules_dir"] do
  to "#{Chef::Config[:file_cache_path]}/runbooks/alerts"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
end

link node["prometheus"]["recording_rules_dir"] do
  to "#{Chef::Config[:file_cache_path]}/runbooks/recordings"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
end

link node["prometheus"]["rules_dir"] do
  to "#{Chef::Config[:file_cache_path]}/runbooks/rules"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
end

directory node["prometheus"]["inventory_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

# Manage the inventory directory to cleanup any old templates.
managed_directory node["prometheus"]["inventory_dir"] do
  action :clean
end

ark ::File.basename(node["prometheus"]["dir"]) do
  url node["prometheus"]["binary_url"]
  checksum node["prometheus"]["checksum"]
  version node["prometheus"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path ::File.dirname(node["prometheus"]["dir"])
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  #  extension node['prometheus']['file_extension'] unless node['prometheus']['file_extension'].empty?
  action :put
  notifies :restart, "runit_service[prometheus]", :delayed
end

# prometheus.tar.gz package ships a default 'consoles' directory
# so we need to remove it before doing the symlink
directory node["prometheus"]["console_templates_dir"] do
  action :delete
  recursive true
  not_if { File.symlink?(node["prometheus"]["console_templates_dir"]) }
end

link node["prometheus"]["console_templates_dir"] do
  to "#{Chef::Config[:file_cache_path]}/runbooks/consoles"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
end

if node["prometheus"]["external_labels"]["replica"].nil?
  # Extract the replica number from the hostname.
  replica = node.name[/prometheus-[a-z-]*(\d{2,}).*/, 1]
  node.default["prometheus"]["external_labels"]["replica"] = replica unless replica.nil?
end

config = {
  "global" => {
    "scrape_interval" => node["prometheus"]["scrape_interval"],
    "scrape_timeout" => node["prometheus"]["scrape_timeout"],
    "evaluation_interval" => node["prometheus"]["evaluation_interval"],
    "external_labels" => node["prometheus"]["external_labels"],
  },
  "rule_files" => node["prometheus"]["rule_files"],
  "alerting" => node["prometheus"]["alerting"],
  "scrape_configs" => parse_jobs(node["prometheus"]["jobs"], node["prometheus"]["inventory_dir"]),
}

file "Prometheus config" do
  path node["prometheus"]["flags"]["config.file"]
  content hash_to_yaml(config)
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
  notifies :hup, "runit_service[prometheus]"
end

# Generate job inventory files.
node["prometheus"]["jobs"].each do |name, conf|
  public_hosts = conf["public_hosts"] || []
  exporter_port = conf["exporter_port"] || 80

  search_query = nil
  search_query = [conf["role_name"]].flatten.map { |role_name| "roles:#{role_name}" }.join(" OR ") if conf["role_name"]
  search_query = conf["chef_search"] if conf["chef_search"]
  next if search_query.nil?

  query = search(:node, search_query).sort! { |a, b| a[:fqdn] <=> b[:fqdn] }

  inventory_filepath = File.join(node["prometheus"]["inventory_dir"], "#{conf["inventory_file_name"] || name}.yml")

  file inventory_filepath do
    content generate_inventory_file(query, exporter_port, public_hosts).to_yaml
    owner node["prometheus"]["user"]
    group node["prometheus"]["group"]
    mode "0644"
  end
end

include_recipe "runit::default"
runit_service "prometheus" do
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["prometheus"]["log_dir"]
end

node.default["thanos-sidecar"]["enable"] = true
include_recipe "gitlab-prometheus::thanos"
