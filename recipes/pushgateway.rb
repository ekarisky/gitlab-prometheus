include_recipe "gitlab-prometheus::default"

node.default["ark"]["package_dependencies"] = []
include_recipe "ark::default"

dir_name = ::File.basename(node["pushgateway"]["dir"])
dir_path = ::File.dirname(node["pushgateway"]["dir"])

ark dir_name do
  url node["pushgateway"]["url"]
  checksum node["pushgateway"]["checksum"]
  version node["pushgateway"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
  notifies :restart, "service[pushgateway]", :delayed
end

systemd_unit "pushgateway.service" do
  content <<~END_UNIT
            [Unit]
            Description=Prometheus Pushgateway
            After=network.target

            [Service]
            ExecStart=#{node["pushgateway"]["dir"]}/pushgateway
    User=#{node["prometheus"]["user"]}

    [Install]
    WantedBy=multi-user.target
          END_UNIT
  action %i(create enable)
  notifies :restart, "service[pushgateway]", :delayed
end

service "pushgateway" do
  action %i(enable start)
end
