include_recipe "gitlab-prometheus::default"

ark "thanos-binary" do
  url node["thanos"]["binary_url"]
  checksum node["thanos"]["checksum"]
  version node["thanos"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path ::File.dirname(node["thanos"]["dir"])
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :put
end

include_recipe "runit::default"

directory node["thanos-sidecar"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-sidecar"]["enable"] }
end

thanos_search = node["thanos"]["cluster_search"]
thanos_cluster_port = node["thanos"]["cluster_port"]
node.default["thanos"]["peers"] = search(:node, thanos_search).map { |n|
  [n["ipaddress"], thanos_cluster_port].join(":")
}.sort.uniq

runit_service "thanos-sidecar" do
  options(
    thanos_mode: "sidecar",
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-sidecar"]["log_dir"]
  subscribes :restart, "ark[thanos-binary]", :delayed
  only_if { node["thanos-sidecar"]["enable"] }
end

directory node["thanos-query"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-query"]["enable"] }
end

runit_service "thanos-query" do
  options(
    thanos_mode: "query",
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-query"]["log_dir"]
  subscribes :restart, "ark[thanos-binary]", :delayed
  only_if { node["thanos-query"]["enable"] }
end

directory node["thanos-store"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-store"]["enable"] }
end

runit_service "thanos-store" do
  options(
    thanos_mode: "store",
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-store"]["log_dir"]
  subscribes :restart, "ark[thanos-binary]", :delayed
  only_if { node["thanos-store"]["enable"] }
end

directory node["thanos-compact"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
  only_if { node["thanos-compact"]["enable"] }
end

runit_service "thanos-compact" do
  options(
    thanos_mode: "compact",
  )
  run_template_name "thanos"
  default_logger true
  sv_timeout 120 # Bumping from the default 7 seconds
  log_dir node["thanos-compact"]["log_dir"]
  subscribes :restart, "ark[thanos-binary]", :delayed
  only_if { node["thanos-compact"]["enable"] }
end
