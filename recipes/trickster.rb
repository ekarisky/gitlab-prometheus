include_recipe "gitlab-prometheus::default"

archive_file = "/tmp/trickster-#{node["trickster"]["version"]}.gz"
binary_file = node["trickster"]["binary"]

directory node["trickster"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory node["trickster"]["cache_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

remote_file archive_file do
  source node["trickster"]["binary_url"]
  checksum node["trickster"]["checksum"]
  notifies :run, "execute[extract-trickster]", :immediately
end

execute "extract-trickster" do
  command "test ! -f '#{binary_file}' || mv '#{binary_file}' '#{binary_file}.bak' && gunzip -c #{archive_file} > #{binary_file} && chmod 755 #{binary_file}"
  user node["prometheus"]["user"]
  group node["prometheus"]["group"]
  action :nothing
  notifies :restart, "systemd_unit[trickster.service]"
end

template node["trickster"]["flags"]["config"] do
  source "trickster.conf.erb"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
  notifies :restart, "systemd_unit[trickster.service]"
end

trickster_cmd = [binary_file, Gitlab::Prometheus.flags_for(node, "trickster")].join(" ")

trickster_unit = {
  Unit: {
    Description: "Trickster",
    Documentation: ["https://github.com/Comcast/trickster"],
    After: "network.target",
  },
  Service: {
    Type: "simple",
    ExecStart: trickster_cmd,
    KillMode: "process",
    MemoryLimit: "#{node["trickster"]["memory_kb"]}K",
    Restart: "always",
    RestartSec: "5s",
    User: node["prometheus"]["user"],
  },
  Install: {
    WantedBy: "multi-user.target",
  },
}

systemd_unit "trickster.service" do
  content trickster_unit
  action [:create, :enable, :start]
end
