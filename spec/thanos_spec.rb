require "spec_helper"

describe "gitlab-prometheus::thanos" do
  context "default execution" do
    before do
      stub_search(:node, "recipes:gitlab-prometheus\\:\\:thanos").and_return(
        [
          {"fqdn" => "fist_node",
           "hostname" => "my.hostname",
           "ipaddress" => "10.0.0.1"},
          {"fqdn" => "fist_node",
           "hostname" => "labeled.hostname",
           "ipaddress" => "10.0.0.2"},
        ]
      )
    end

    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.override["thanos-sidecar"]["enable"] = true
        node.override["thanos-query"]["enable"] = true
        node.override["thanos-store"]["enable"] = true
        node.override["thanos-compact"]["enable"] = true
      }.converge(described_recipe)
    end

    it "creates the log dir in the configured location" do
      expect(chef_run).to create_directory("/var/log/prometheus/thanos-sidecar").with(
        owner: "prometheus",
        group: "prometheus",
        mode: "0755",
        recursive: true,
      )
    end

    let(:node) { chef_run.node }

    it "populates a cluster peers list" do
      expect(node["thanos"]["peers"]).to eq(["10.0.0.1:10900", "10.0.0.2:10900"])
    end

    it "includes runit::default" do
      expect(chef_run).to include_recipe("runit::default")
    end

    it "runs the thanos-sidecar service" do
      expect(chef_run).to enable_runit_service("thanos-sidecar").with(
        default_logger: true,
        log_dir: "/var/log/prometheus/thanos-sidecar",
      )
    end
  end
end
