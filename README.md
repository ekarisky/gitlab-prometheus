gitlab-prometheus Cookbook
==========================

This cookbook installs and sets up prometheus and its related exporters.

Requirements
------------
TODO: List your cookbook requirements. Be sure to include any requirements this cookbook has on platforms, libraries, other cookbooks, packages, operating systems, etc.


Attributes
----------
TODO: List your cookbook attributes here.


Usage
-----

#### gitlab-prometheus::prometheus

This recipe installs and configures prometheus instance

##### Installation

By default prometheus installed in `/opt/prometheus/prometheus` directory. Version controlled by `["prometheus"]["version"]` attribute. With changing version of prometheus to install also change `["prometheus"]["checksum"]` attribute (sha256sum).

##### Configuration

###### 1. Alertmanager configuration
Configured by searching for nodes running `recipe:gitlab-prometheus\:\:alertmanager` and  the `["alertmanager"]["port"]` (9093) attribute.

###### 2. Configuring jobs
Jobs can be configured in prometheus role config in chef-repo by defining entry in `['prometheus']['jobs']` attribute.

```
"prometheus" : {
  "jobs" : {
    "runners-cache-registry" : {
      "scrape_interval": "60s",
      "scrape_timeout": "59s",
      "metrics_path": "/probe",
      "target": [ "127.0.0.1:5000","127.0.0.1:9000"],
      ...
    }
  }
}
```

The following standard Prometheus attributes can be used inside a job definition:

- `scrape_interval`
- `scrape_timeout`
- `honor_labels`
- `params`
- `relabel_configs`
- `metric_relabel_configs`
- `metrics_path`

The following special attributes can be used inside a job definition:

Generate `static_sd_configs`:

- `target`

Generate `file_sd_configs`:

- `file_inventory`
- `inventory_file_name`
- `exporter_port`
- `public_hosts`

See: Inventory generators below.

Generate `consul_sd_configs`:

- `consul_inventory`

Attributes are used to generate [scrape_config](https://prometheus.io/docs/operating/configuration/#%3Cscrape_config%3E) definitions.

Inventory generators:
- `chef_search`
- `role_name`

These generators create [file_sd_config](https://prometheus.io/docs/operating/configuration/#<file_sd_config>) inventory files.  These files can use `inventory_file_name` to generate teh file, or default to `role_name`.  The result will include the labels `instance=<hostname>`, `fqdn=<fqdn>`.

Targets inside this file are scraped by internal ip (if exists), otherwise it is scraped by external ip and by port specified in `exporter_port`. You can override this functionality by putting fqdn value of host to `public_hosts` array attribute. Nodes in this array are scraped by `fqdn` values.


If `chef_search` is declared, a chef search will be run to generate the inventory.

If `role_name` is declared, the generated file will contain every node for that role. 

NOTE: If there are two or more elements with same `role_name`, only one inventory file will be created, rendering one of the configs useless. In this case, be sure to use `inventory_file_name` key with any reasonable value that doesn't conflict with existing inventory files.

If `consul_inventory` is declared, chef will generate the `consul_sd_configs` entry. `consul_inventory` entry is a hash with `server` string, defining the address under which Consul cluster is available, and `services` array defining which services available at the Consul cluster should be handled by Prometheus.

## Labeling nodes

Any node that is subject to be scaped by prometheus can be labeled just by adding the following structure to the node itself:

```json
  "prometheus": {
    "labels": {
      "environment": "prod",
      "tier": "fe"
    }
  }
```

The labels are free form and can be set, for example, in the role. The way this values will be accessed later on is like this:

```ruby
node["prometheus"]["labels"] = { "environment": "prod", "tier": "fe" }
```
