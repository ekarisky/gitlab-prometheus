#
# Cookbook Name GitLab::Monitoring
# Attributes:: default
#

default["prometheus"]["user"] = "prometheus"
default["prometheus"]["group"] = "prometheus"
default["prometheus"]["alertmanagers_search"] = "recipes:gitlab-alertmanager\\:\\:default"
