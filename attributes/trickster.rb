default["trickster"]["version"] = "0.1.1"
default["trickster"]["checksum"] = "6b77bd4e1e3a3709a004a6f432395e413d938c0a255b42d05863570dca08d593"

default["trickster"]["dir"] = "/opt/prometheus/trickster"
default["trickster"]["log_dir"] = "/var/log/prometheus/trickster"
default["trickster"]["cache_dir"] = "#{node["trickster"]["dir"]}/cache"

default["trickster"]["binary_url"] = "https://github.com/Comcast/trickster/releases/download/v#{node["trickster"]["version"]}/trickster-#{node["trickster"]["version"]}.linux-amd64.gz"

default["trickster"]["binary"] = "#{node["trickster"]["dir"]}/trickster"
default["trickster"]["flags"]["config"] = "#{node["trickster"]["dir"]}/trickster.conf"

# Allocate 80% of memory to Trickster by default.
default["trickster"]["memory_kb"] = (node["memory"]["total"].to_i * 0.8).to_i

# Configuration params
default["trickster"]["proxy_server"]["listen_port"] = 9095
default["trickster"]["proxy_server"]["listen_address"] = "127.0.0.1"
default["trickster"]["cache"]["cache_type"] = "filesystem"
default["trickster"]["cache"]["record_ttl_secs"] = 3600
default["trickster"]["cache"]["reap_sleep_ms"] = 1000
default["trickster"]["cache"]["compression"] = "true"
default["trickster"]["cache"]["options"] = {
  "filesystem" => {
    "cache_path" => node["trickster"]["cache_dir"],
  },
}
default["trickster"]["origins"] = {}
default["trickster"]["metrics"]["listen_port"] = 9195
default["trickster"]["metrics"]["listen_address"] = "0.0.0.0"
default["trickster"]["logging"]["log_level"] = "info"
